import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home'
import Ad from './components/Ads/Ad'
import AdList from './components/Ads/AdList'
import NewAd from './components/Ads/NewAd'
import Login from './components/Auth/Login'
import Registration from './components/Auth/Registration'
import Orders from './components/User/Orders'
import UserProfile from "./components/User/UserProfile";
import UserProfileEdit from "./components/User/UserProfileEdit";
// import UserProfileEdit from "./components/User/UserProfileOptions";
import NotFound from "./components/Auth/NotFound";
import AuthCheck from "./routerGuard"


Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            meta: {requiresAuth: true}
        },
        {
            path: '/ad/:id',
            props: true,
            name: 'ad',
            component: Ad,
            beforeEach: AuthCheck
        },
        {
            path: '/list',
            name: 'AdList',
            component: AdList,
            beforeEach: AuthCheck
        },
        {
            path: '/new',
            name: 'newAd',
            component: NewAd,
            beforeEach: AuthCheck
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/registration',
            name: 'registration',
            component: Registration
        },
        {
            path: '/userProfile',
            name: 'userProfile',
            component: UserProfile,
            beforeEach: AuthCheck,
        },
        {
            path: '/userProfileEdit',
            name: 'userProfileEdit',
            props: true,
            component: UserProfileEdit,
            // component: UserProfileOptions,
            beforeEach: AuthCheck
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders,
            beforeEach: AuthCheck,
        },
        {
            path: '/about',
            name: 'about',
            // beforeEach: AuthCheck
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            // component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '*',
            component: NotFound
        },
    ],
    mode: 'history',

})


