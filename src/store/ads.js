import * as fb from 'firebase'

class Ad {
    constructor(ownerId, title, description, promo = false, imgSrc = '', id = null,mainPage = false, price = 0,rating=0,category=null) {
        this.ownerId = ownerId;
        this.title = title;
        this.description = description;
        this.promo = promo;
        this.imgSrc = imgSrc;
        this.id = id;
        this.mainPage = mainPage;
        this.price = price;
        this.rating = rating;
        this.category = category;
    }
}



export default {
    state: {
        ads: []
    },
    mutations: {
        createAd(state, payload) {
            state.ads.push(payload)

        },
        LoadAd(state, payload) {
            state.ads = payload

        },
        deleteAd(state, payload) {
            for (var i = 0; i < state.ads.length; i++) {
                if (state.ads[i].id === payload) {
                    state.ads.splice(i, 1);
                }
            }
        },
        updateAd(state, {id, imgSrc, title, description, promo,  mainPage,  rating, price, category}) {
            const ad = state.ads.find(a => {
                return a.id === id
            })

            if (imgSrc !== '') {
                ad.title = title
                ad.description = description
                ad.imgSrc = imgSrc
                ad.promo = promo
                ad.mainPage = mainPage
                ad.rating = rating
                ad.price = price
                ad.category = category
            } else {
                ad.title = title
                ad.description = description
                ad.promo = promo
                ad.mainPage = mainPage
                ad.rating = rating
                ad.price = price
                ad.category = category
            }

        }

    },
    computed: {
        loading() {
            this.$store.getters.loading
        }
    },
    actions: {
        async createAd({commit, getters}, payload) {

            const image = payload.image;
            commit('clearError');
            commit('setLoading', true);
            try {
                const newAd = new Ad(getters.user.id, payload.title, payload.description, payload.promo, '','' ,payload.mainPage, payload.price, payload.rating, payload.category);
                const fbResponse = await fb.database().ref('ads').push(newAd);
                const fileExtType = image.name.slice(image.name.lastIndexOf('.'));
                const fileData = await fb.storage().ref(`ads/${fbResponse.key}.${fileExtType}`).put(image);
                const imgSrc = await fileData.ref.getDownloadURL();
                const updates = {};
                updates['imgSrc'] = imgSrc;
                fb.database().ref(`ads/${fbResponse.key}`).update(updates);

                commit('setLoading', false);
                commit('createAd', {
                    ...newAd,
                    id: fbResponse.key,
                    imgSrc: imgSrc
                })
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },
        async updateAd({commit}, payload) {
            const title = payload.title;
            const id = payload.id;
            const description = payload.description;
            const promo = payload.promo;
            const image = payload.image;
            const imgSrc = '';
            const mainPage = payload.mainPage;
            const price = payload.price;
            const rating = payload.rating;
            const category = payload.category;


            commit('clearError');
            commit('setLoading', true);
            try {
                if (image !== null) {
                    const fileExtType = image.name.slice(image.name.lastIndexOf('.'));
                    const fileData = await fb.storage().ref(`ads/${id}.${fileExtType}`).put(image);
                    const imgSrc = await fileData.ref.getDownloadURL();
                    await fb.database().ref('ads').child(id).update({
                        imgSrc,
                        title,
                        description,
                        promo,
                        mainPage,
                        rating,
                        price,
                        category,
                    })
                    commit('updateAd', {
                        id,
                        imgSrc,
                        title,
                        description,
                        promo,
                        mainPage,
                        rating,
                        price,
                        category,
                    })
                } else {
                    await fb.database().ref('ads').child(id).update({
                        title,
                        description,
                        promo,
                        mainPage,
                        rating,
                        price,
                        category,
                    })

                    commit('updateAd', {
                        id,
                        imgSrc,
                        title,
                        description,
                        promo,
                        mainPage,
                        rating,
                        price,
                        category,
                    })
                }
                commit('setLoading', false);

            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },
        async fetchAds({commit}) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const resultAds = [];
                const fbResponse = await fb.database().ref('ads').once('value');
                const ads = fbResponse.val();
                Object.keys(ads).forEach(key => {
                    const ad = ads[key];
                    resultAds.push(
                        new Ad(ad.ownerId, ad.title, ad.description, ad.promo, ad.imgSrc, key, ad.mainPage, ad.price, ad.rating,ad.category)
                    )
                });
                commit('LoadAd', resultAds);
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },
        async deleteAd({commit}, payload) {
            commit('clearError');
            commit('setLoading', true);
            try {
                await fb.database().ref(`ads/${payload.id}`).remove();
                commit('deleteAd', payload.id);
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        }
    },
    getters: {
        ads(state) {
            return state.ads;
        },
        promoAds: state => {
            return state.ads.filter(ad => {
                return ad.mainPage === true
            })
        },
        adsByID(state) {
            return adId => {
                return state.ads.find(ad => ad.id === adId)
            }
        },

    }
}