import * as fb from 'firebase'

class Order {
    constructor(title, phone, adId, done = false, userId = null,price=0, picture='',rating = 0, count=0,ordId = '') {
        this.title = title
        this.phone = phone
        this.adId = adId
        this.done = done
        this.userId = userId
        this.price = price
        this.picture = picture
        this.rating = rating
        this.count = count
        this.ordId = ordId
    }
}

export default {
    state: {
        orders: []
    },
    mutations: {
        loadOrders(state, payload) {
            state.orders = payload
        },
        createOrders(state, payload) {
            state.orders.push(payload)
        },
        updateOrdersCount(state, {orderCountId,orderCount}) {
            const ord = state.orders.find(a => {
                return a.ordId === orderCountId
            })
            ord.count = orderCount
        },
        deleteOrder(state, payload){
            for (var i = 0; i < state.orders.length; i++) {
                if (state.orders[i].ordId === payload) {
                    state.orders.splice(i, 1);
                }
            }
        }
    },
    actions: {
        async createOrder({commit}, {title, phone, adId, done, userId , price , picture, rating, count }) {
            const order = new Order(title, phone, adId, done, userId , price , picture, rating, count )
            commit('clearError')
            try {
                const path = `/users/${userId}/orders`;

                let orderCount = 1;
                let orderCountId;
                const fbRsponse = await fb.database().ref(path).once('value')
                const orders = fbRsponse.val();

                if(orders === null){
                    const dataResponse = await fb.database().ref(path).push(order)

                    commit('createOrders', {
                        ...order,
                        ordId: dataResponse.key,
                    })
                }else{
                    Object.keys(orders).forEach(key => {
                        const ord = orders[key];
                        if(ord.adId === adId ){
                            ord.count++;
                            orderCount = ord.count
                            orderCountId = key;
                        }
                    });
                    if(orderCountId !== undefined) {
                        await fb.database().ref(path).child(orderCountId)
                            .update({
                                count: orderCount
                            });
                        commit('updateOrdersCount', {orderCountId,orderCount})
                    }else{
                        const dataResponse = await fb.database().ref(path).push(order)
                        commit('createOrders', {
                            ...order,
                            ordId: dataResponse.key,
                        })
                    }
                }
            } catch (error) {
                commit('setError', error.message);
                throw error
            }
        },

        async fetchOrders({commit}) {
            commit('setLoading', true)
            commit('clearError')
            const resultOrders = [];
            try {
                var user = fb.auth().currentUser;
                const fbRsponse = await fb.database().ref(`/users/${user.uid}/orders`).once('value')
                const orders = fbRsponse.val();
                Object.keys(orders).forEach(key => {
                    const ord = orders[key];
                    resultOrders.push(
                    new Order(ord.title, ord.phone, ord.adId, ord.done, ord.userId , ord.price , ord.picture, ord.rating, ord.count, key)
                    )
                });
                commit('loadOrders', resultOrders)
                commit('setLoading', false)
            } catch (error) {
                commit('setLoading', false)
                // commit('setError', error.message);
            }
        },

        async deleteOrder({commit}, payload) {
            commit('clearError');
            commit('setLoading', true);
            try {
                var user = fb.auth().currentUser;
                await fb.database().ref(`/users/${user.uid}/orders/${payload.ordId}`).remove();
                commit('deleteOrder',  payload.ordId);
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },

        async markOrderByDone({commit}, payload) {

            commit('clearError');
            commit('setLoading', true);
            try {
                var user = fb.auth().currentUser;
                await fb.database().ref(`/users/${user.uid}/orders`).child(payload)
                    .update({
                        done: true
                    });
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },

    },
    getters: {
        doneOrders(state) {
            return state.orders.filter(o => o.done)
        },
        undoneOrders(state) {
            return state.orders.filter(o => !o.done)
        },
        orders(state, getters) {
            return getters.undoneOrders.concat(getters.doneOrders)
        },
        areas(state) {
            return state.areas
        }
    }
}