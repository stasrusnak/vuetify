import * as fb from 'firebase'

class User {
    constructor(id, firstName, lastName, email, addres = '', city = '', country = '', postalCode = 0, about = '', avatar = '', age = 0, chipGroup = [], FB = '', Instagram = '', Linkedin = '') {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.addres = addres;
        this.city = city;
        this.country = country;
        this.postalCode = postalCode;
        this.about = about;
        this.avatar = avatar;
        this.age = age;
        this.chipGroup = chipGroup;
        this.FB = FB;
        this.Instagram = Instagram;
        this.Linkedin = Linkedin;
    }
}

export default {
    state: {
        user: null
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload
        },
        updateUser(state, param) {
                state.user.firstName = param.firstName
                state.user.lastName = param.lastName
                state.user.avatar = param.avatar
                state.user.age = param.age
                state.user.chipGroup = param.chipGroup
                state.user.email = param.email
                state.user.about = param.about
                state.user.FB = param.FB
                state.user.Instagram = param.Instagram
                state.user.Linkedin = param.Linkedin
        }
    },
    actions: {
        async fetchUser({commit}) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const us = fb.auth().currentUser;
                const id = us.uid;
                const path = `/users/${id}/profile`;
                const Respon = await fb.database().ref(path).once('value');

                const ads = Respon.val();
                // eslint-disable-next-line no-console
                commit('setUser', new User(ads.id, ads.firstName, ads.lastName, ads.email, ads.addres, ads.city, ads.country, ads.postalCode, ads.about, ads.avatar, ads.age, ads.chipGroup, ads.FB, ads.Instagram, ads.Linkedin));
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },

        async registerUser({commit}, user) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const users = await fb.auth().createUserWithEmailAndPassword(user.user.email, user.user.password)
                const person = new User(users.user.uid, user.user.firstName, user.user.lastName, user.user.email, '', '', '', null, '', '', null)
                commit('setUser', person);
                const path = `/users/${users.user.uid}/profile`;
                await fb.database().ref(path).set(person)
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },
        async loginUser({commit}, user) {
            commit('clearError');
            commit('setLoading', true);
            try {
                const users = await fb.auth().signInWithEmailAndPassword(user.email, user.password)
                commit('setUser', new User(users.user.uid,
                    users.user.firstName,
                    users.user.lastName,
                    users.user.email,
                    users.user.addres,
                    users.user.city,
                    users.user.country,
                    users.user.postalCode,
                    users.user.about,
                    users.user.avatar,
                    users.user.age));
                commit('setLoading', false);
            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },


        async UpdateUserProflie({commit}, payload) {
            const image = payload.image;
            const about = payload.About
            const chipGroup = payload.chipGroup
            const age = payload.age
            const firstName = payload.userFirstName
            const lastName = payload.userlastName
            const FB = payload.FB
            const Instagram = payload.Instagram
            const Linkedin = payload.Linkedin
            const user = fb.auth().currentUser;
            const id = user.uid
            const path = `/users/${id}/profile`;

            commit('clearError');
            commit('setLoading', true);
            try {
                const param = {
                    'avatar': "",
                    'about': about,
                    'chipGroup': chipGroup,
                    'age': age,
                    'firstName': firstName,
                    'lastName': lastName,
                    'FB': FB,
                    'Instagram': Instagram,
                    'Linkedin': Linkedin,
                    'id': id,
                }

                if (image !== null) {
                    const fileExtType = image.name.slice(image.name.lastIndexOf('.'));
                    const fileData = await fb.storage().ref(`ads/${id}.${fileExtType}`).put(image);
                    const res = await fileData.ref.getDownloadURL();
                    param['avatar'] = res
                }

                for (let [key, value] of Object.entries(param)) {
                    if (value === 0 || value === [] || value === null || value === "") {
                        delete param[`${key}`];
                    }
                }
                await fb.database().ref(path).update(param);
                commit('updateUser', param)
                commit('setLoading', false);

            } catch (error) {
                commit('setError', error.message);
                commit('setLoading', false);
                throw error
            }
        },
        autologin({commit}, payload) {
            commit('setUser', new User(payload.uid))

        },
        logOutUser({commit}) {
            fb.auth().signOut();
            commit('setUser', null);
        },
    },
    getters: {
        user(state) {
            return state.user;
        },
        isUserLogging(state) {
            return state.user !== null
        }

    }
}