import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import VueMask from 'v-mask'


Vue.use(Vuetify);
Vue.use(VueMask);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    dark: false,
    options: {
      customProperties: true,
    },
  },



});



