import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store';
import vuetify from './plugins/vuetify';
import * as fb from 'firebase'
import BuyModalComponent from "./components/Shared/BuyModal";
import Dashboard from "./components/Shared/Dashboard";
import License from "./components/Shared/License";
import UserProfileMenu from "./components/User/UserProfileMenu";
import VCardTemplate from "./components/Shared/VCardTemplate";
import Treeview from "./components/Shared/treeview";
import Map from "./components/Shared/Map";
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';
import { Icon } from 'leaflet';

delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});



const connectOptions={
    apiKey: process.env.VUE_APP_API_KEY,
    authDomain: process.env.VUE_APP_AUTH_DOMAIN,
    databaseURL: process.env.VUE_APP_DATA_BASE_URL,
    projectId: process.env.VUE_APP_PROJECT_ID,
    storageBucket: process.env.VUE_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_MESSAGING_SENDER_ID,
    appId: process.env.VUE_APP_ID,
    measurementId: process.env.VUE_APP_MEASUREMENT_ID,
};

fb.initializeApp(connectOptions);

const unsubscribe =  fb.auth().onAuthStateChanged(user => {
        new Vue({
            router,
            store,
            vuetify,
            render: h => h(App),
            created () {
                if (user) {
                    this.$store.dispatch('autologin',user);
                }
                this.$store.dispatch('fetchAds');
                this.$store.dispatch('fetchUser')
                this.$store.dispatch('fetchOrders')
            }
        }).$mount('#app')
        unsubscribe()
    })
Vue.component('add-buy-modal', BuyModalComponent);
Vue.component('user-profile-menu', UserProfileMenu);
Vue.component('v-treeview-menu', Treeview);
Vue.component('dashboard', Dashboard);
Vue.component('license', License);
Vue.component('v-card-template', VCardTemplate);

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('v-map', Map);


